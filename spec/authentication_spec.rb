RSpec.describe 'User authentication: ', type: :feature do

  name = Faker::Name.name
  email = Faker::Internet.email
  let (:driver) { Watir::Browser.new :chrome }
  let (:radiochat) { driver.div(class: 'radiochat') }
  let (:login_link) { driver.link(href: /login/) }
  let (:logout_link) { driver.link(href: /logout/) }
  let (:signup_link) { driver.link(href: /signup/) }

  def signup(name, email, pwd='password', pwd_conf='password')
    unless driver.div(id: 'error_explanation').present?
      driver.a(id: 'navbarDropdown').click
      driver.link(href: /signup/).click
    end
    driver.text_field(id: 'user_name').value = name
    driver.text_field(id: 'user_email').value = email
    driver.text_field(id: 'user_password').value = pwd
    driver.text_field(id: 'user_password_confirmation').value = pwd_conf
    driver.button(text: "Зарегистрироваться").click
  end

  def login(email, pwd='password')
    driver.a(id: 'navbarDropdown').click
    driver.link(href: /login/).click
    driver.text_field(id: 'session_email').set(email)
    driver.text_field(id: 'session_password').set('password')
    driver.button(text: "Авторизоваться").click
  end

  def number_of_errors
    error_msg = driver.div(id: 'error_explanation')
    number_of_errors = error_msg.child(tag_name: 'ul').to_a.length
  end

  before do
    driver.goto "https://apm-radio.herokuapp.com"
  end

  after do
    driver.close
  end

  it 'signs up with valid data' do
    expect(radiochat.present?).to eq(false)
    signup(name, email)
    expect(driver.url).to match(/https:\/\/apm-radio.herokuapp.com\/users\/[0-9]+/)
    driver.goto "https://apm-radio.herokuapp.com"
    expect(radiochat.present?).to eq(true)
  end

  # checks the login feature and chat presence
  it 'logins with valid data followed by logout' do
    expect(radiochat.present?).to eq(false)
    login(email)
    expect(driver.url).to eq('https://apm-radio.herokuapp.com/')
    expect(radiochat.present?).to eq(true)
    driver.a(id: 'navbarDropdown').click
    # user logs out
    driver.link(href: /logout/).click
    expect(radiochat.present?).to eq(false)
    driver.a(id: 'navbarDropdown').click
    expect(login_link.present?).to eq(true)
    expect(signup_link.present?).to eq(true)
  end

  it 'signs up with invalid data' do
    # with already taken name
    signup(name, email)
    expect(number_of_errors).to eq(1)
    # with not matching passwords
    password = 'passw0rd'
    signup(name, email, password)
    expect(number_of_errors).to eq(2)
    # with short password (less than 6 char)
    password = pwd_conf = 'fooba'
    signup(name,email,password, pwd_conf)
    expect(number_of_errors).to eq(2)
    # with not valid email
    email = 'not_validemail@sssss'
    signup(name, email)
    expect(number_of_errors).to eq(2)
    driver.goto("https://apm-radio.herokuapp.com")
    expect(radiochat.present?).to eq(false)
  end
end
